var path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: "./src/js/main.ts",
  output: {
    path: path.resolve(__dirname, "build", "js"),
    filename: "scottkgregory.bundle.js"
  },
  watch: true,
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"]
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: "./src/", to: "../../build/", ignore: ["js/**.*"] }
    ])
  ],
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      }
    ]
  }
};
