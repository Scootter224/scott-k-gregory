import { generateTextBounds } from "./utils/style";
import { randomInt } from "./utils/numbers";
import Bubble from "./shapes/bubble";
import State from "./state";

const state = State.getInstance();

window.addEventListener("StateReady", event => {
  let ctx = state.canvas.getContext("2d")!;
  let style_height = +getComputedStyle(state.canvas)
    .getPropertyValue("height")
    .slice(0, -2);
  let style_width = +getComputedStyle(state.canvas)
    .getPropertyValue("width")
    .slice(0, -2);
  var height: number = style_height * state.dpr;
  var width: number = style_width * state.dpr;
  state.canvas.height = height;
  state.canvas.width = width;
  const centreX = width / 2;
  const centreY = height / 2;

  state.renderCanvas.height = height;
  state.renderCanvas.width = width;
  let ctxRender = state.renderCanvas.getContext("2d")!;

  ctx.lineJoin = "round";
  ctx.lineCap = "round";
  ctx.textBaseline = "middle";
  ctx.font = `${state.fontWeight} ${state.fontSize} ${state.font}`;
  ctx.textAlign = "center";
  ctxRender.lineJoin = "round";
  ctxRender.lineCap = "round";
  ctxRender.textBaseline = "middle";
  ctxRender.font = `${state.fontWeight} ${state.fontSize} ${state.font}`;
  ctxRender.textAlign = "center";
  state.logoWidth = ctx.measureText(state.logoText).width * state.dpr;

  var letterBounds = generateTextBounds(
    centreX,
    centreY,
    ctxRender,
    state.renderCanvas.width,
    state.renderCanvas.height
  );

  var tick = 0;
  var points: Bubble[] = [];

  var clear = function () {
    ctx.clearRect(0, 0, state.canvas.width, state.canvas.height);
  };
  var a = 0;
  var redetectTick = 26
  var loop = function () {
    window.requestAnimationFrame(loop);
    clear();

    points.forEach(point => point.draw(ctx));

    if (state.stopped >= state.letterThreshold) {
      state.allStopped = true;
      state.stoppedTicks += 1;

      if (state.stoppedTicks == 50) {
        state.logoText = state.words[randomInt(0, 5)];
        var newBounds = generateTextBounds(
          centreX,
          centreY,
          ctxRender,
          state.renderCanvas.width,
          state.renderCanvas.height
        );
        redetectTick = tick + 100
        state.detectLetter = false;
        state.stoppedTicks = 0;
        state.stopped = 0;
        state.allStopped = false;
        points.forEach(point => {
          point.stopIn = newBounds;
          point.speedX = point.initialSpeedX;
          point.speedY = point.initialSpeedY;
          if (point.stop) {
            point.color = [
              state.colorQuaternary,
              state.colorTertiary,
              state.colorSecondary,
              state.colorPrimary
            ][randomInt(0, 3)],
              point.stop = false;
          }
        });
      }
    }

    if (tick == redetectTick) state.detectLetter = true;
    tick++;
  };

  for (let i = 0; i < state.bubbleCount; i++) {
    points.push(
      new Bubble(
        state.bubbleMaxSpeed,
        state.canvas.width,
        state.canvas.height,
        state.bubbleMinSize,
        state.bubbleMaxSize,
        letterBounds,
        [
          state.colorQuaternary,
          state.colorTertiary,
          state.colorSecondary,
          state.colorPrimary
        ][randomInt(0, 3)],
        null,
        // state.colorAccent
      )
    );
  }

  loop();
});
