import State from "../state";

export const remToPixels = (rem: number) => {
  var docFontSize: number = parseFloat(
    getComputedStyle(document.documentElement).fontSize
  );
  return rem * docFontSize;
};

export const parseRgb = (rgb: string) => {
  var result = rgb.replace(/[^\d,]/g, "").split(",");
  return result
    ? {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16)
    }
    : null;
};

export const generateTextBounds = (
  centreX: number,
  centreY: number,
  ctx: CanvasRenderingContext2D,
  w: number,
  h: number
) => {
  ctx.clearRect(0, 0, w, h)
  const state = State.getInstance();
  // ctx.font = `${state.fontWeight} ${state.fontSize} ${state.font}`;
  // ctx.textBaseline = "middle";
  // ctx.fillText(state.logoText, centreX, centreY);
  const image = <CanvasImageSource>document.getElementById('source')!;
  image.width = image.width * state.dpr;
  image.height = image.height * state.dpr;
  ctx.drawImage(image, centreX - 250 * state.dpr, centreY - 250 * state.dpr);

  var imageData = ctx.getImageData(0, 0, w, h).data;
  // var data32 = new Uint32Array(imageData.data.buffer);
  var p: { [key: string]: { stopped: number, color: string } } = {};

  var x = 0;
  for (var i = 0; i < imageData.length; i += 4) {
    const r = imageData[i];
    const g = imageData[i + 1];
    const b = imageData[i + 2];
    const a = imageData[i + 3];
    if (a > 0) {
      p[`${Math.floor(x % w)}|${Math.floor((x / w) | 0)}`] = { stopped: 0, color: `rgb(${r},${g},${b})` };
    }
    x += 1;
  }
  return p;
};
